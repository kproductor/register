package com.kaufland.kproductor;

import com.kaufland.kproductor.dto.ManufacturerDTO;
import com.kaufland.kproductor.dto.ProductDTO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RegisterServiceTest {

    @Autowired
    RegisterService registerService;

    @Test
    public void findBestProductTest001() {
        registerService.manufacturers.clear();

        ProductDTO prod = new ProductDTO(50, 20, 30);
        ManufacturerDTO manu1 = new ManufacturerDTO("0.0.0.0:1", "TestManu1", prod);

        registerService.manufacturers.put(manu1.address, manu1);
        ManufacturerDTO manu = registerService.findBestProduct(prod);

        Assert.assertEquals(manu1, manu);
    }

    @Test
    public void findBestProductTest002() {
        for (int i = 0; i < 100; i++) {
            registerService.manufacturers.clear();

            ProductDTO prod1 = new ProductDTO(50, 30, 20);
            ManufacturerDTO manu1 = new ManufacturerDTO("0.0.0.0:1", "TestManu1", prod1);
            ProductDTO prod2 = new ProductDTO(30, 30, 40);
            ManufacturerDTO manu2 = new ManufacturerDTO("0.0.0.0:2", "TestManu2", prod2);

            registerService.manufacturers.put(manu1.address, manu1);
            registerService.manufacturers.put(manu2.address, manu2);
            ManufacturerDTO manu = registerService.findBestProduct(new ProductDTO(60, 20, 20));

            Assert.assertEquals(manu1, manu);
        }
    }

    @Test
    public void findBestProductTest003() {
        for (int i = 0; i < 100; i++) {
            registerService.manufacturers.clear();

            ProductDTO prod1 = new ProductDTO(50, 30, 20);
            ManufacturerDTO manu1 = new ManufacturerDTO("0.0.0.0:1", "TestManu1", prod1);
            ProductDTO prod2 = new ProductDTO(30, 30, 40);
            ManufacturerDTO manu2 = new ManufacturerDTO("0.0.0.0:2", "TestManu2", prod2);

            registerService.manufacturers.put(manu1.address, manu1);
            registerService.manufacturers.put(manu2.address, manu2);
            ManufacturerDTO manu = registerService.findBestProduct(new ProductDTO(40, 30, 30));

            Assert.assertTrue(manu.equals(manu1) || manu.equals(manu2));
        }
    }

    @Test
    public void getManufacturerDTOs001() {
        registerService.manufacturers.clear();

        ProductDTO prod1 = new ProductDTO(50, 30, 20);
        ManufacturerDTO manu1 = new ManufacturerDTO("0.0.0.0:1", "TestManu1", prod1);
        ProductDTO prod2 = new ProductDTO(30, 30, 40);
        ManufacturerDTO manu2 = new ManufacturerDTO("0.0.0.0:2", "TestManu2", prod2);

        registerService.manufacturers.put(manu1.address, manu1);
        registerService.manufacturers.put(manu2.address, manu2);

        Assert.assertEquals(2, registerService.getManufacturerDTOs().size());
        registerService.getManufacturerDTOs().stream().forEach(pManufacturerDTO -> Assert.assertTrue(pManufacturerDTO instanceof ManufacturerDTO));
    }
}
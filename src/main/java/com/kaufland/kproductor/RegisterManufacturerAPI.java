package com.kaufland.kproductor;

import com.kaufland.kproductor.dto.ManufacturerDTO;
import com.kaufland.kproductor.dto.ProductDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

@CrossOrigin(origins = "*")
@RestController()
public class RegisterManufacturerAPI {

    private static Logger LOGGER = Logger.getLogger(RegisterManufacturerAPI.class);

    @Autowired
    RegisterService registerService;

    @PostMapping("/api/manufacturer")
    public void registerManufacturer(@RequestBody ManufacturerDTO manufacturer) {
        LOGGER.info("registerManufacturer() manufacturer=" + manufacturer.toString());
        registerService.registerManufacturer(manufacturer);
    }

    @GetMapping("/api/manufacturer")
    public Collection<ManufacturerDTO> getManufacturers() {
        LOGGER.info("getManufacturers() " + registerService.getManufacturerDTOs());
        return registerService.getManufacturerDTOs();
    }

    @PostMapping("/api/order")
    public ResponseEntity sendOrder(@RequestParam int quality, @RequestParam int costs, @RequestParam int time) {
        ProductDTO product = new ProductDTO(quality, costs, time);
        ManufacturerDTO manufacturer = registerService.findBestProduct(product);
        if (manufacturer != null) {
            LOGGER.info("manufacturer=" + manufacturer.toString() + " |  product=" + product.toString());
            RestTemplate restTemplate = new RestTemplate();
            return restTemplate.postForEntity(manufacturer.address + "/api/order", product, ResponseEntity.class);
        } else {
            LOGGER.info("sendOrder() no manufacturer");
            return ResponseEntity.noContent().build();
        }
    }
}
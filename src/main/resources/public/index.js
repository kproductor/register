function getManufacturers() {
    $.ajax({
        url: window.location.origin + "/api/manufacturer"
    }).done(function (data) {
        console.log("Success getManufacturers");
        console.log(data)
        var html = "";
        data.forEach(function (manufacturer) {
            html += "<div class='row'>" +
                "<div class='col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3'>" + manufacturer.name + "</div>" +
                "<div class='col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3'>" + manufacturer.product.quality + "</div>" +
                "<div class='col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3'>" + manufacturer.product.costs + "</div>" +
                "<div class='col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3'>" + manufacturer.product.time + "</div>" +
                "</div>";
        });
        $("#manufacturersContainer").html(html);
    });
}

function registerLocaleDummyManufacturer() {
    $.ajax({
        type: "POST",
        url: window.location.origin + "/api/manufacturer",
        data: JSON.stringify({address: "0.0.0.0:1", name: "Not functional Test", product: {quality: 0, costs: 0, time: 0}}),
        contentType: "application/json; charset=utf-8"
    }).done(function () {
        console.log("Success registerLocaleDummyManufacturer");
    }).fail(function (data) {
        console.log("Fail");
        console.log(data);
    });
}

function sendOrder() {
    $.ajax({
        type: "POST",
        url: window.location.origin + "/api/order" +
        "?quality=" + $("#qualityInput").val() +
        "&costs=" + $("#costsInput").val() +
        "&time=" + $("#timeInput").val()
    }).done(function (data) {
        console.log("Success sendOrder");
        console.log(data);
    }).fail(function (data) {
        console.log("Fail");
        console.log(data);
    });
}

function sendOrderToAll() {
    $.ajax({
        url: window.location.origin + "/api/manufacturer"
    }).done(function (data) {
        console.log("Success sendOrderToAll");
        console.log(data)
        /*var options = "";
         dtos.forEach(function (dto) {
         options += "<li><a href=\"#\" onclick=\"selectElements(this, \'" + apiPath + "\',\'" +
         drawComponentBenchmarkFunction + "\',\'" + drawGameBenchmarkFunction + "\');\">" + dto.name + "</a></li>"

         });
         var dataList = input.closest("#selector").find("#datalist");
         dataList.html(options);
         if (!dataList.parent().hasClass("open")) {
         dataList.dropdown("toggle");
         }*/
    });
}
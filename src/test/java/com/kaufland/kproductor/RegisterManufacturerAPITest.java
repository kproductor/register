package com.kaufland.kproductor;

import com.kaufland.kproductor.dto.ManufacturerDTO;
import com.kaufland.kproductor.dto.ProductDTO;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RegisterManufacturerAPITest {

    Logger LOGGER = Logger.getLogger(RegisterManufacturerAPITest.class);

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    RegisterService registerService;

    @Test
    public void registerManufacturerTest001() {
        registerService.manufacturers.clear();
        ManufacturerDTO manu = new ManufacturerDTO("0.0.0.0:1", "TestManu1", new ProductDTO(50, 30, 20));
        ResponseEntity<String> response = this.restTemplate.postForEntity("/api/manufacturer", manu, String.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        LOGGER.info(response);
    }

    @Test
    public void getManufacturersTest001() {
        registerService.manufacturers.clear();
        ResponseEntity<ArrayList> response = this.restTemplate.getForEntity("/api/manufacturer", ArrayList.class);
        LOGGER.info(response);
    }

    @Test
    public void registerAndGetManufacturersTest002() {
        registerService.manufacturers.clear();
        ManufacturerDTO manu1 = new ManufacturerDTO("0.0.0.0:1", "TestManu1", new ProductDTO(50, 30, 20));
        Assert.assertEquals(HttpStatus.OK, this.restTemplate.postForEntity("/api/manufacturer", manu1, String.class).getStatusCode());

        ResponseEntity<ArrayList> response = this.restTemplate.getForEntity("/api/manufacturer", ArrayList.class);
        LOGGER.info(response.getBody());
    }

    @Test
    public void sendOrder() {

    }
}
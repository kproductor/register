package com.kaufland.kproductor;

import com.kaufland.kproductor.dto.ManufacturerDTO;
import com.kaufland.kproductor.dto.ProductDTO;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
public class RegisterService {
    public ConcurrentHashMap<String, ManufacturerDTO> manufacturers = new ConcurrentHashMap<>();

    public ManufacturerDTO findBestProduct(ProductDTO searchProduct) {
        if (CollectionUtils.isEmpty(manufacturers.values())) {
            return null;
        }
        int bestDifference = Integer.MAX_VALUE;
        List<ManufacturerDTO> bestManufacturers = new LinkedList<>();
        for (ManufacturerDTO manufacturer : manufacturers.values()) {
            int tmpDifference = searchProduct.differenceToOther(manufacturer.product);
            if (bestDifference > tmpDifference) {
                bestManufacturers.clear();
                bestManufacturers.add(manufacturer);
                bestDifference = tmpDifference;
            } else if (bestDifference > tmpDifference) {
                bestManufacturers.add(manufacturer);
            }
        }
        return bestManufacturers.get((int) (Math.random() * bestManufacturers.size()));
    }

    public void registerManufacturer(ManufacturerDTO pManufacturer) {
        manufacturers.put(pManufacturer.address, pManufacturer);
    }

    public Collection<ManufacturerDTO> getManufacturerDTOs() {
        return manufacturers.values().stream().map(pManufacturer -> (ManufacturerDTO) pManufacturer).collect(Collectors.toList());
    }
}
